import re
from openapi.model import (
    Swagger,
    Info,
    Operation,
    PathItem,
    Paths,
    Response,
    Responses,
    ParametersList,
    PathParameterSubSchema,
    MediaTypeList,
)
from zope.interface import implementer
from .interfaces import IOpenAPIGenerator


def includeme(config):
    config.register_service_factory(OpenAPISchemaGenerator, IOpenAPIGenerator)


@implementer(IOpenAPIGenerator)
class OpenAPISchemaGenerator:
    def __init__(self, context, request):
        self.context = context
        self.introspector = request.registry.introspector

    def __call__(self):
        return self.generate()

    def generate(self):
        return Swagger(
            swagger="2.0",
            info=Info(
                title="Example",
                version="1.0.0",
            ),
            basePath="/",
            paths=Paths(self.generate_paths()),
            consumes=MediaTypeList([
                "application/json",
                "application/x-www-form-urlencoded",
            ]),
        )

    def generate_paths(self):
        routes = self.introspector.get_category("routes")
        for route_intr in routes:

            intr = route_intr["introspectable"]

            if intr["static"]:
                continue

            pattern = intr["pattern"]
            path_parameters = self.get_path_parameters(pattern)
            operations = self.generate_operations(route_intr, path_parameters)
            if not operations:
                continue

            yield (pattern, PathItem(operations))

    def generate_operations(self, route_intr, path_parameters):
        operations = {
                r.get("request_methods").lower(): Operation(
                    description=self.get_description(r["callable"], r["attr"]),
                    parameters=ParametersList(
                        path_parameters +
                        self.generate_form_parameters(r["callable"], r["attr"]),
                    ),
                    responses=Responses({
                        "200": Response(description="dummy"),
                        })
                    ) for r in route_intr["related"] if r.category_name == 'views' and r.get("request_methods")
            }
        return operations

    def get_path_parameters(self, pattern):        
        urlvars = re.findall(r"{(\w+)}", pattern)
        path_parameters = [
            PathParameterSubSchema({
                "name": v,
                "required": True,
                "in": "path",
                "type": "string",
            })
            for v in urlvars]
        return path_parameters

    def generate_form_parameters(self, view_callable, attr=None):
        view = view_callable
        if attr is not None:
            view = getattr(view, attr)

        params = self.introspector.get("openapi.parameter_schema", view)
        if params is None:
            return []
        return params["value"]

    def get_description(self, view_callable, attr=None):
        view = view_callable
        if attr is not None:
            view = getattr(view, attr)
        return view.__doc__ or ""
