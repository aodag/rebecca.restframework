from .interfaces import IOpenAPIGenerator


def includeme(config):
    config.add_static_view(name='swagger-ui-static', path="rebecca.restframework:static/swagger-ui-2.2.8")
    config.add_route('swagger-ui', '/swagger-ui')
    config.add_view(swaggerui, route_name='swagger-ui', renderer="templates/swagger-ui/index.html")
    config.add_route('swagger', '/swagger')
    config.add_view(swagger, route_name='swagger', renderer="json")


def swaggerui(request):
    return dict()


def swagger(request):
    generator = request.find_service(IOpenAPIGenerator)
    swagger = generator()
    return swagger
