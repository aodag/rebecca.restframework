from zope.interface import Interface


class IOpenAPIGenerator(Interface):
    def __call__():
        """generate openapi.model.Swagger"""
