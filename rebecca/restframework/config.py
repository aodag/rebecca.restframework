import inspect


def includeme(config):
    config.add_directive("add_rest_view", add_rest_view)
    config.add_directive("add_rest_route", add_rest_route)
    config.add_directive("add_parameter_schema", add_parameter_schema)


CRUD_MAP = [
    ('GET', 'index', 'collection'),
    ('POST', 'create', 'collection'),
    ('GET', 'detail', 'member'),
    ('PUT', 'update', 'member'),
    ('DELETE', 'delete', 'member'),
]

CRUD_METHOD_NAMES = [method for http_method, method, kind in CRUD_MAP]


def create_route_names(resource_name):
    collection_route = "%s.collection" % resource_name
    member_route = "%s.member" % resource_name
    action_route ="%s.action" % resource_name
    routes = {
        "collection": collection_route,
        "member": member_route,
        "action": action_route,
    }
    return routes


def add_rest_route(config, resource_name):
    routes = create_route_names(resource_name)
    config.add_route(routes["collection"],
                     "/%s/" % resource_name)
    config.add_route(routes["member"],
                     "/%s/{id}" % resource_name)
    config.add_route(routes["action"],
                     "/%s/{id}/actions/{action}" % resource_name)


def add_rest_view(config, resource_name, view):
    view = config.maybe_dotted(view)
    routes = create_route_names(resource_name)

    for request_method, method, route in CRUD_MAP:
        if not hasattr(view, method):
            continue
        config.add_view(
            view,
            attr=method,
            request_method=request_method,
            route_name=routes[route],
            renderer="json",
        )

    for method, _ in inspect.getmembers(view):
        if method.startswith("_"):
            continue
        if method in CRUD_METHOD_NAMES:
            continue
        config.add_view(
            view,
            attr=method,
            request_method="POST",
            route_name=routes["action"],
            renderer="json",
            match_param="action=%s" % method,
        )


def add_parameter_schema(config, view, schema, attr=None):
    view = config.maybe_dotted(view)
    if attr is not None:
        view = getattr(view, attr)

    intr = config.introspectable(
        category_name="openapi.parameter_schema",
        discriminator=view,
        title="schema for %s" % view.__name__,
        type_name="dict",
    )
    intr["value"] = schema
    intr["view"] = view
    # print(config.introspector.get_category('views'))
    # intr.relate("views", view)

    config.action('openapi parameter schema', introspectables=(intr,))
