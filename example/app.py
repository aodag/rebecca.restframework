from pyramid.config import Configurator


class ExampleView:
    def __init__(self, request):
        self.request = request

    def index(self):
        """example index view"""
        return {"message": "Hello"}

    def create(self):
        name = self.request.params.get("name")
        return {"name": name}

    def detail(self):
        return {"message": "Hello"}


def main(global_conf, **settings):
    from openapi.model import ParametersList, FormDataParameterSubSchema
    config = Configurator(
        settings=settings,
    )
    config.include('pyramid_services')
    config.include('pyramid_jinja2')
    config.include('pyramid_debugtoolbar')
    config.add_jinja2_renderer('.html')
    config.include('rebecca.restframework')
    config.add_rest_route("example")
    config.add_rest_view("example", ExampleView)
    config.add_parameter_schema(
        ExampleView,
        ParametersList([
            FormDataParameterSubSchema({
                "name": "name",
                "type": "string",
                "in": "formData",
            }),
        ]),
        attr="create")
    return config.make_wsgi_app()


def application():
    settings = {
        'pyramid.reload_templates': True,
        'pyramid.reload_assets': True,
    }
    return main({}, **settings)
