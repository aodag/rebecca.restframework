import pytest
import webtest


class TestFunctional:
    @pytest.fixture
    def target(self):
        from rebecca.restframework.interfaces import IOpenAPIGenerator
        class DummyView:
            def __init__(self, request):
                self.request = request
            
            def index(self):
                self.request.response.text = "index"
                return self.request.response

            def create(self):
                self.request.response.text = "create"
                return self.request.response

            def detail(self):
                id = self.request.matchdict["id"]
                self.request.response.text = "detail of %s" % id
                return self.request.response

            def update(self):
                id = self.request.matchdict["id"]
                self.request.response.text = "update of %s" % id
                return self.request.response

            def delete(self):
                id = self.request.matchdict["id"]
                self.request.response.text = "delete of %s" % id
                return self.request.response

            def finish(self):
                self.request.response.text = "finish"
                return self.request.response

            def activate(self):
                self.request.response.text = "activate"
                return self.request.response

        def swagger_view(request):
            generator = request.find_service(IOpenAPIGenerator)
            swagger = generator()
            return swagger


        from pyramid.config import Configurator
        from openapi.model import (
            ParametersList,
            FormDataParameterSubSchema,
        )
        config = Configurator()
        config.include("pyramid_services")
        config.include("pyramid_jinja2")
        config.add_jinja2_renderer(".html")
        config.include('rebecca.restframework')
        config.add_rest_route('dummy')
        config.add_rest_view("dummy", DummyView)
        config.add_parameter_schema(DummyView, ParametersList([
            FormDataParameterSubSchema({
                "name": "name",
                "type": "string",
                "in": "formData",
            }),
        ]), attr="create")
        config.add_route('swagger', '/swagger')
        config.add_view(swagger_view, route_name="swagger", renderer="json")
        app = config.make_wsgi_app()
        return webtest.TestApp(app)

    def test_swagger(self, target):
        import openapi
        result = target.get('/swagger')
        swagger = openapi.loads(result.text)
        swagger.validate()

    def test_swagger_ui(self, target):
        target.get('/swagger-ui')

    def test_index(self, target):
        target.get('/', status=404)

    def test_dummy_index(self, target):
        result = target.get('/dummy/')
        assert result.text == "index"

    def test_dummy_create(self, target):
        result = target.post('/dummy/')
        assert result.text == "create"

    def test_dummy_detail(self, target):
        result = target.get('/dummy/1')
        assert result.text == 'detail of 1'

    def test_dummy_update(self, target):
        result = target.put('/dummy/1')
        assert result.text == 'update of 1'

    def test_dummy_delete(self, target):
        result = target.delete('/dummy/1')
        assert result.text == 'delete of 1'

    def test_dummy_finish(self, target):
        result = target.post('/dummy/1/actions/finish')
        assert result.text == "finish"
